from uncertainties import ufloat
import numpy as np
from scipy.optimize import minimize as minimize
from functools import partial as partial

def get_erange(val,err):
    t1 = ufloat(val,err)
    minimizer1 = lambda t,x: (1000*np.exp(t*x) - 1)**2
    range1 = minimize(partial(minimizer1,t1.n),1).x
    range1plus = minimize(partial(minimizer1,t1.n+t1.s),1).x
    range1minus = minimize(partial(minimizer1,t1.n-t1.s),1).x
    return (range1[0],range1plus[0],range1minus[0])

def erange_fromfit(ddresult):
    t1 = get_erange(ddresult.params['t1'].value,ddresult.params['t1'].stderr)
    t2 = get_erange(ddresult.params['t2'].value,ddresult.params['t2'].stderr)
    print(f'{t1[0]:.2f}+{t1[1]-t1[0]:.2f}-{t1[0]-t1[2]:.2f}')
    print(f'{t2[0]:.2f}+{t2[1]-t2[0]:.2f}-{t2[0]-t2[2]:.2f}')
    return (t1,t2)